using System;
using System.Collections.Generic;
using System.Text;

namespace Menu
{
    public class Price
    {
        public string currency { set; get; }
        public double? amount { set; get; }

        public Price(string currency, double? amount)
        {
            this.currency = currency;
            this.amount = amount;
            
        }


    }
}
