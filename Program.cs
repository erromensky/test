using System;
using System.Collections.Generic;
using System.Linq;

namespace Menu
{
    class MenuProvider
    {

        public List<Product> productList = new List<Product>();
        public List<Category> cat = new List<Category>();
        public Dictionary<string, double> course = new Dictionary<string, double>(); 



        public string rub = "Rub";
        public string euro = "Euro";
        public string dollar = "Dollar";

        double rubDol = 62.3;
        double rubEuro = 82.1;
        double euroDol = 1.3;



        public void AddProduct(int id, string name, double? price, string currency, Category catOb) 
        {
            var product = new Product();
            product.SetId(id);
            product.SetProdName(name);
            product.SetPrice(price);
            product.SetCurrency(currency);
            product.AddCategory(catOb);
            product.SetPrice(price);
            productList.Add(product);
        }


       public void DeleteProduct(int id)
        {
            Product DelProduct = GetProduct(id);
            productList.Remove(DelProduct);
        }


        public List<Product> GetProducts()
        {
            return productList;
        }


        public Product GetProduct(int id)
        {
            Product getProduct = productList.Where(i => i.id == id).FirstOrDefault();
            return getProduct;
        }

        public List<Product> GetProductsByCatId(int catId)
        {

            return productList.Where(i =>i.ProductInCat(catId)).ToList();

             //List<Product> productResults = new List<Product>();
            //return productResults;
        }


        public void DeleteProductPrice(double? DeletePrice)
        {

           List<Product> delPriceList = new List<Product>();

            delPriceList= productList.Where(i => i.priceProd.amount == DeletePrice).ToList();

            foreach (var p in delPriceList)
            {
                if (p.priceProd.amount == DeletePrice)
                {
                    p.priceProd.amount = null;
                }
            }    
        }


        public void CourseCurrency()
        {
            course.Add("DollarEuro", 0.9);
            course.Add("DollarRub", 63.2);
            course.Add("EuroDollar", 1.11);
            course.Add("EuroRub", 74.4);
            course.Add("RubDollar", 0.4);
            course.Add("RubEuro", 0.2);
            course.Add("RubRub", 1);
            course.Add("EuroEuro", 1);
            course.Add("DollarDollar", 1);        
        }


        public void ConvertCurrency(string currencyNameIn)
        {
            foreach (var pr in productList)
            {
                string currencyName = pr.priceProd.currency + currencyNameIn;
                double ratio = course.Where(i => i.Key == currencyName).First().Value;
                pr.priceProd.amount = pr.priceProd.amount * ratio;
                pr.priceProd.currency = currencyNameIn;
            }      
        }


        public double? SumCurrency(string currencyNameIn)
        {

            var sum = productList.Select(i => new
            {
                PriceProduct = i.priceProd.amount,
                Ratio = course[i.priceProd.currency + currencyNameIn]
            }
            );

            return sum.Sum(s => s.PriceProduct * s.Ratio);
        }

        public double? MinPrice(string currencyNameIn)
        {
            var sum = productList.Select(i => new
            {
                PriceProduct = i.priceProd.amount,
                Ratio = course[i.priceProd.currency + currencyNameIn]
            }
                );
            return sum.Min(s => s.PriceProduct * s.Ratio);
        }


        public double? MaxPrice(string currencyNameIn)
        {
            var sum = productList.Select(i => new
            {
                PriceProduct = i.priceProd.amount,
                Ratio = course[i.priceProd.currency + currencyNameIn]
            }
                );
            return sum.Max(s => s.PriceProduct * s.Ratio);
        }


        public bool CheckPrice(string currencyNameIn)
        {
            var sum = productList.Select(i => new
            {
                PriceProduct = i.priceProd.amount,
                Ratio = course[i.priceProd.currency + currencyNameIn]
            }
                );

           return sum.All(s => s.PriceProduct * s.Ratio > 2);
        }


        public void ProdInfo()
        {
            foreach (var c in cat) 
            {
                Console.WriteLine($"\n Category: {c.name}");
                var infoProd = GetProductsByCatId(c.id);
                ConsoleWriteInfo(infoProd);
            }
            Console.WriteLine("--------------------------");

        }


        public void ConsoleWriteInfo(List<Product>products)
        {
            foreach (var prod in products)
            {
                string catName = null;
                foreach (var i in prod.categoryList.OrderBy(i=>i.sort)) 
                { 
                    catName += i.name + " ";             
                }
                Console.WriteLine($"ProdId: {prod.id} ProdName: {prod.name}  Price: {String.Format("{0:0.00}", prod.priceProd.amount)} {prod.priceProd.currency} Category: {catName}");
            }
        }


 
        public double? SummPrice(int catId, string currency)
        {
            double? summ = 0;


            var spisok = GetProductsByCatId(catId);

            foreach (var p in spisok)
            {
                if (currency == dollar)
                   
                {
                    if (p.priceProd.currency == rub)
                    {
                        summ += p.priceProd.amount / rubDol;

                    }
                    if (p.priceProd.currency == euro)
                    {
                        summ += p.priceProd.amount / euroDol;

                    }
                    if (p.priceProd.currency == dollar)
                    {
                        summ += p.priceProd.amount;

                    }
                }

                if (currency == euro)
                {
                    if (p.priceProd.currency == rub)
                    {
                        summ += p.priceProd.amount / rubEuro;

                    }
                    if (p.priceProd.currency == dollar)
                    {
                        summ += p.priceProd.amount * euroDol;

                    }
                    if (p.priceProd.currency == euro)
                    {
                        summ += p.priceProd.amount;

                    }
                }

                if (currency == rub)
                {
                    if (p.priceProd.currency == dollar)
                    {
                        summ += p.priceProd.amount * rubDol;

                    }
                    if (p.priceProd.currency == euro)
                    {
                        summ += p.priceProd.amount * rubEuro;
                    }
                    if (p.priceProd.currency == rub)
                    {
                        summ += p.priceProd.amount;
                    }
                }
            }
            return summ;
        }


        public void ConvertProductsPrice(string currency)
        {
           foreach (var p in productList)
            {
                Product productNew = p;

                if (currency == dollar)
                {
                    if (p.priceProd.currency == rub)
                    {
                        p.priceProd.amount = p.priceProd.amount / rubDol;
                        p.priceProd.currency = currency;

                    }
                    if (p.priceProd.currency == euro)
                    {
                        p.priceProd.amount = p.priceProd.amount / euroDol;
                        p.priceProd.currency = currency;

                    }
                }

                if (currency == euro)
                {
                    if (p.priceProd.currency == rub)
                    {
                        p.priceProd.amount = p.priceProd.amount / rubEuro;
                        p.priceProd.currency = currency;

                    }
                    if (p.priceProd.currency == dollar)
                    {
                        p.priceProd.amount = p.priceProd.amount * euroDol;
                        p.priceProd.currency = currency;

                    }
                }

                if (currency == rub)
                {
                    if (p.priceProd.currency == dollar)
                    {
                        p.priceProd.amount = p.priceProd.amount * rubDol;
                        p.priceProd.currency = currency;

                    }
                    if (p.priceProd.currency == euro)
                    {
                        p.priceProd.amount = p.priceProd.amount * rubEuro;
                        p.priceProd.currency = currency;
                    }
                }
            }
        }

  

        static void Main(string[] args)
        {
            MenuProvider menu = new MenuProvider();
            
            Category cat1 = new Category(1, "dinner", 4);     
            Category cat2 = new Category(2, "lunch", 1);     
            Category cat3 = new Category(3, "breakfast", 3);    
            Category cat4 = new Category(4, "supper", 2);    


            menu.cat.Add(cat1);
            menu.cat.Add(cat2);
            menu.cat.Add(cat3);
            menu.cat.Add(cat4);

            
            menu.AddProduct(1, "moloko", 10, menu.rub, cat1);
            menu.AddProduct(2, "sous", 10, menu.euro, cat1);
            menu.AddProduct(3, "xleb", 25, menu.dollar, cat2);
            menu.AddProduct(4, "tea", 45, menu.dollar, cat2);
            menu.AddProduct(5, "coffe", 12, menu.rub, cat3);
            menu.AddProduct(6, "bbq", 89, menu.euro, cat3);       
            menu.AddProduct(7, "egg", 12, menu.dollar, cat4);




            var addCat = menu.GetProduct(1);
            addCat.AddCategory(cat4);
            addCat.AddCategory(cat3);

            var addCat2 = menu.GetProduct(2);
            addCat2.AddCategory(cat3);
            addCat2.AddCategory(cat4);


            menu.ProdInfo();

            menu.CourseCurrency();
            menu.ConvertCurrency(menu.dollar);

           // menu.ProdInfo();

            var checkP = menu.GetProducts();
            menu.ConsoleWriteInfo(checkP);

            var check = menu.GetProducts().Any(i => i.priceProd.currency == "Rub");
            Console.WriteLine(check);
            Console.WriteLine("--------1--------");


            // menu.DeleteProductPrice(10);
            // menu.GetProduct(1000);
            //   menu.CourseCurrency();
            //   menu.ConvertCurrency(menu.dollar);
            
            var minProd = menu.MinPrice(menu.dollar);
            var maxProd = menu.MaxPrice(menu.dollar);
            var checkPrice = menu.CheckPrice(menu.dollar);
            Console.WriteLine($" Min price: {String.Format("{0:0.00}", minProd)} \n Max price {String.Format("{0:0.00}", maxProd)}  {checkPrice} {menu.dollar}");

            
            foreach (var prod in menu.GetProducts())
            {
                string catName = null;
                foreach (var i in prod.categoryList)
                {
                    catName += i.name + " ";
                }
                Console.WriteLine(prod.id + " " + prod.name + " " + String.Format("{0:0.00}", prod.priceProd.amount) + " " + prod.priceProd.currency + " " + catName);

            }
            Console.WriteLine("--------1--------");

            var summa1 = menu.SumCurrency(menu.dollar);
            Console.WriteLine(String.Format("{0:0.00}", summa1) + " " + menu.dollar);
            Console.WriteLine("---------2------------");

            

            Console.WriteLine("--------11--------");

            // добавление категории к товару
            // var addCat2 = menu.GetProduct(1);
            // addCat2.AddCategory(cat4);


            // изменение категорий товара
            var changeCat = menu.GetProduct(5);
            changeCat.AddCategory(cat2);
            foreach (var p in menu.GetProducts())
            {
                if (p.ProductInCat(cat3.id) && p.ProductInCat(cat2.id))
                {
                    p.DelCategory(cat2.id);
                    p.DelCategory(cat3.id);
                    p.AddCategory(cat4);
                }
            }


     
            //Сумма товаров в категории в указанной валюте
            var summa = menu.SummPrice(cat1.id, menu.rub);
            Console.WriteLine(summa + " " + menu.rub);
            Console.WriteLine("-------3-------");

            var pr = menu.GetProduct(4);
            pr.AddCategory(cat3);

            var newCat = menu.GetProductsByCatId(cat3.id);
            foreach (var c in newCat)
            {
                var newName = c.categoryList.Where(i => i.id == cat3.id).FirstOrDefault();
                newName.name = "after dinner";
            
            }

            menu.ConvertProductsPrice(menu.euro);
            
                foreach (var prod in menu.GetProducts())
                {
                    string catName = null;
                    foreach (var i in prod.categoryList)
                    {
                    catName += i.name + " ";
                    }
                Console.WriteLine(prod.id + " " + prod.name + " " + String.Format("{0:0.00}", prod.priceProd.amount) + " " + prod.priceProd.currency + " " + catName);

            }

 



            // удаление заданной цены
           // menu.DeleteProductPrice(10);

            /*
            menu.DeleteProduct(4);
            foreach (var prod in menu.GetProducts())
            {
               Console.WriteLine(prod.id + " " + prod.name + " " + prod.priceProd.amount);
            }
            
            Console.WriteLine("---------------------");
                                 

            var prod1 = menu.GetProduct(1);
            var prod2 = menu.GetProduct(100);

            Console.WriteLine(prod1.name);
            Console.WriteLine("-- --");
            Console.WriteLine(prod2.name);

*/


        }
    }
}
