using System;
using System.Collections.Generic;
using System.Text;

namespace Menu
{
    public class Category
    {
        public int id { set; get; }
        public string name { set; get; }

        public int sort { set; get; }

        public Category(int catId, string catName, int sort)
        {
            this.id = catId;
            this.name = catName;
            this.sort = sort;
        
        }


    }
}
