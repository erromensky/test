using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Menu
{
    class Product
    {
        public int id { set; get; }
        public string name { set; get; }

        public Price priceProd = new Price(null, null);
        public List<Category> categoryList = new List<Category>();
       // public Dictionary<int, string> categoryDictionary = new Dictionary<int, string>();


        public Product()
        {
            
        }

        public void SetId(int id)
        {
            this.id = id;
        }

        public void SetProdName(string name)
        {
            this.name = name;
        }
        public void SetPrice(double? amount)
        {
            priceProd.amount = amount;
        }

        public void SetCurrency(string currency)
        {
            priceProd.currency = currency;
        }


        public void AddCategory(Category cat)
        {
            if (!categoryList.Any(i=>i.id == cat.id))
            {
                categoryList.Add(cat);
            }
        }


        public void DelCategory(int catId)
        {
            var delCat = categoryList.Where(i => i.id == catId).FirstOrDefault();
            categoryList.Remove(delCat);
        }

        public bool ProductInCat(int catId)
        {            
            return categoryList.Any(i => i.id == catId);
        }
    }


}





